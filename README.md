# TempPerm #

Du möchtest Leuten auf deinem Server Rechte nur für eine bestimmte Zeit geben? Dann ist dieses Plugin genau richtig für dich! 

## Wie funktioniert es und wie benutze ich es? ##

Also, es ist ganz einfach du gibst den Befehl /tempperm <Player> <Permission> <Zeit> ein, dann bekommt der Spieler die Rechte bis die Zeit abläuft.

## Installation ##

Ganz einfach, Drag ´n Drop in den plugins Ordner! Du benötigst außerdem PermissionsEx.

## Rechte ##

Zum benutzen des Befehles brauchst du die Permission tempperm.use oder OP.

## TempPerm ist OpenSource! ##

[Klicke hier, um auf das BitBucket Repository zu gelangen!](https://bitbucket.org/LeLyfa/tempperm/)
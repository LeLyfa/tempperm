package de.lelyfa.tempperm.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.lelyfa.tempperm.Main;
import de.lelyfa.tempperm.utils.Language;
import de.lelyfa.tempperm.utils.Utils;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Tempperm implements CommandExecutor {

	private Main pl;

	public Tempperm(Main pl) {
		this.pl = pl;
	}

	String prefix = Utils.getPrefix();

	@Override
	public boolean onCommand(CommandSender cs, Command arg1, String arg2, String[] args) {
		if (cs.hasPermission("tempperm.use")) {
			if (args.length == 3) {
				if (Bukkit.getPlayer(args[0]) != null) {

					Player z = Bukkit.getPlayer(args[0]);

					try {
						String s = args[2];
						final int time = Integer.parseInt(s);

						String perm = args[1];

						go(z, time, perm);

					} catch (NumberFormatException nfe) {
						Language.usage(cs);
						return true;
					}

				} else {
					Language.playerIsntOnline(cs);
				}
			} else {
				Language.usage(cs);
			}
		} else{
			Language.noPerm(cs);
		}
		return false;
	}

	private void go(final Player z, final int time, final String perm) {
		PermissionsEx.getUser(z).addPermission(perm);
		
		Language.sendGegeben(z, perm, ""+time);
		Language.sendBekommen(z, perm, ""+time);

		Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {

			@Override
			public void run() {
				PermissionsEx.getUser(z).removePermission(perm);

				Language.sendAbgelaufen(z, perm, ""+time);
				Language.sendWeggenommen(z, perm);

			}
		}, time * 1200L);

	}

}

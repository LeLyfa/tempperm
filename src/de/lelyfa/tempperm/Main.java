package de.lelyfa.tempperm;

import org.bukkit.plugin.java.JavaPlugin;

import de.lelyfa.tempperm.commands.Tempperm;
import de.lelyfa.tempperm.utils.Utils;

public class Main extends JavaPlugin{
	@Override
	public void onEnable() {
		initial();
		loadCommands();
		super.onEnable();
	}

	private void initial() {
		Utils.initialConfigs();
		
	}

	private void loadCommands() {
		Tempperm tempperm = new Tempperm(this);
		getCommand("tempperm").setExecutor(tempperm);
	
	}
}

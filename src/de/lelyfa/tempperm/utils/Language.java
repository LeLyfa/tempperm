package de.lelyfa.tempperm.utils;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Language {
	
	static File german = Filemanager.getFile("plugins/TempPerm/languages", "german.yml");
	static File english = Filemanager.getFile("plugins/TempPerm/languages", "english.yml");
	static File config = Filemanager.getFile("plugins/TempPerm", "config.yml");
	
	public static String getLanguage() {
		return Filemanager.getValue(config, "language");
	}

	public static void noPerm(CommandSender cs) {
		if (getLanguage().equalsIgnoreCase("english")) {
			cs.sendMessage(Utils.getPrefix()
					+ ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "noPermissions")));
		} else if (getLanguage().equalsIgnoreCase("german")) {
			cs.sendMessage(Utils.getPrefix()
					+ ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(german, "keineBerechtigungen")));
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			cs.sendMessage(Utils.getPrefix()
					+ ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "noPermissions")));
		}

	}

	public static void playerIsntOnline(CommandSender cs) {
		if (getLanguage().equalsIgnoreCase("english")) {
			cs.sendMessage(Utils.getPrefix()
					+ ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "playerIsntOnline")));
		} else if (getLanguage().equalsIgnoreCase("german")) {
			cs.sendMessage(Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&',
					Filemanager.getValue(german, "SpieleristNichtOnline")));
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			cs.sendMessage(Utils.getPrefix()
					+ ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "playerIsntOnline")));
		}
	}

	public static void usage(CommandSender cs) {
		if (getLanguage().equalsIgnoreCase("english")) {
			cs.sendMessage(
					Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "usage")));
		} else if (getLanguage().equalsIgnoreCase("german")) {
			cs.sendMessage(Utils.getPrefix()
					+ ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(german, "benutzung")));
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			cs.sendMessage(
					Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "usage")));
		}
	}

	public static void sendGegeben(Player p, String perm, String time) {
		if (getLanguage().equalsIgnoreCase("english")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "gavePermission"));
			m = m.replace("[TIME]", time);
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", p.getName());
			Bukkit.broadcast(m, "tempperm.use");
		} else if (getLanguage().equalsIgnoreCase("german")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(german, "berechtigungGegeben"));
			m = m.replace("[ZEIT]", time);
			m = m.replace("[BERECHTIGUNG]", perm);
			m = m.replace("[SPIELER]", p.getName());
			Bukkit.broadcast(m, "tempperm.use");
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "gavePermission"));
			m = m.replace("[TIME]", time);
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", p.getName());
			Bukkit.broadcast(m, "tempperm.use");
		}
		
	}

	public static void sendBekommen(Player p, String perm, String time) {
		if (getLanguage().equalsIgnoreCase("english")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "becamePermission"));
			m = m.replace("[TIME]", time);
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", p.getName());
			p.sendMessage(m);
		} else if (getLanguage().equalsIgnoreCase("german")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(german, "berechtigungBekommen"));
			m = m.replace("[ZEIT]", time);
			m = m.replace("[BERECHTIGUNG]", perm);
			m = m.replace("[SPIELER]", p.getName());
			p.sendMessage(m);
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "becamePermission"));
			m = m.replace("[TIME]", time);
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", p.getName());
			p.sendMessage(m);
		}
		
	}
	
	public static void sendWeggenommen(Player cs, String perm) {
		if (getLanguage().equalsIgnoreCase("english")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "noPermissionsAnymore"));
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", cs.getName());
			cs.sendMessage(m);
		} else if (getLanguage().equalsIgnoreCase("german")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(german, "berechtigungWeggenommen"));
			m = m.replace("[BERECHTIGUNG]", perm);
			m = m.replace("[Spieler]", cs.getName());
			cs.sendMessage(m);
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "noPermissionsAnymore"));
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", cs.getName());
			cs.sendMessage(m);
		}
		
	}
	
	public static void sendAbgelaufen(Player cs, String perm, String time) {
		if (getLanguage().equalsIgnoreCase("english")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "permissionDeleted"));
			m = m.replace("[TIME]", time);
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", cs.getName());
			cs.sendMessage(m);
		} else if (getLanguage().equalsIgnoreCase("german")) {
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(german, "berechtigungAbgelaufen"));
			m = m.replace("[ZEIT]", time);
			m = m.replace("[BERECHTIGUNG]", perm);
			m = m.replace("[SPIELER]", cs.getName());
			cs.sendMessage(m);
		} else {
			System.out.println(" ");
			System.out.println(" The config.yml of the plugin TempPerm is invalid!");
			System.out.println(" The language setting were set to english");
			System.out.println(" ");
			Filemanager.setValue(config, "language", "english");
			String m = Utils.getPrefix() + ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(english, "permissionDeleted"));
			m = m.replace("[TIME]", time);
			m = m.replace("[PERMISSION]", perm);
			m = m.replace("[PLAYER]", cs.getName());
			cs.sendMessage(m);
		}
		
	}

}

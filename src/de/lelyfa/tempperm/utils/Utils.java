package de.lelyfa.tempperm.utils;

import java.io.File;

import org.bukkit.ChatColor;

public class Utils {

	static File german = Filemanager.getFile("plugins/TempPerm/languages", "german.yml");
	static File english = Filemanager.getFile("plugins/TempPerm/languages", "english.yml");
	static File config = Filemanager.getFile("plugins/TempPerm", "config.yml");

	public static void initialConfigs() {

		Filemanager.setStandard(config, "prefix", "&1[&bTemp&3Perm&1]");
		Filemanager.setStandard(config, "language", "english");

		german();
		english();

	}

	private static void english() {
		Filemanager.setStandard(english, "noPermissions", "&cYou don't have enough permissions!");
		Filemanager.setStandard(english, "playerIsntOnline", "&cThe Player is not online!");
		Filemanager.setStandard(english, "usage", "&cUsage: /tempperm <Player> <permission> <time>");
		Filemanager.setStandard(english, "gavePermission",
				"&aThe Player &f[PLAYER] &ahas now the permission &3[PERMISSION] &afor &b[TIME] &aminutes!");
		Filemanager.setStandard(english, "becamePermission",
				"&aNow you have the permission &3[PERMISSION] &afor &b[TIME] &aminutes!");
		Filemanager.setStandard(english, "permissionDeleted",
				"&aThe Player &f[PLAYER] &adoesn't have anymore the permission &3[PERMISSION]&a!");
		Filemanager.setStandard(english, "noPermissionsAnymore",
				"&aNow you don't have the permission &3[PERMISSION] &aanymore!");

	}

	private static void german() {
		Filemanager.setStandard(german, "keineBerechtigungen", "&cDu hast nicht gen�gend Berechtigungen!");
		Filemanager.setStandard(german, "spieleristNichtOnline", "&cDer Spieler ist nicht online!");
		Filemanager.setStandard(german, "benutzung", "&cBenutzung: /tempperm <Spieler> <Berechtigung> <Zeit>");
		Filemanager.setStandard(german, "berechtigungGegeben",
				"&aDer Spieler &f[SPIELER] &ahat nun &b[ZEIT] &aMinuten lang die Berechtigung &3[BERECHTIGUNG] &a!");
		Filemanager.setStandard(german, "berechtigungBekommen",
				"&aDu hast nun &b[ZEIT] &aMinuten lang die Berechtigung &3[BERECHTIGUNG] &a!");
		Filemanager.setStandard(german, "berechtigungAbgelaufen",
				"&aDer Spieler &f[SPIELER] &ahat nun nicht mehr die Berechtigung &3[BERECHTIGUNG] &a!");
		Filemanager.setStandard(german, "berechtigungWeggenommen",
				"&aDu hast nun nicht mehr die Berechtigung &3[BERECHTIGUNG] &a!");

	}

	public static String getPrefix() {
		return ChatColor.translateAlternateColorCodes('&', Filemanager.getValue(config, "prefix") + " ");
	}


}

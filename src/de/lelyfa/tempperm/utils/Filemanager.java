package de.lelyfa.tempperm.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Filemanager {
	
	public static File getFile(String path, String filename){
		return new File(path, filename);
	}
	
	public static FileConfiguration getConfiguration(File file){
		return YamlConfiguration.loadConfiguration(file);
	}
	
	public static void setStandard(File file, String path, String value){
		
		FileConfiguration cfg = getConfiguration(file);
		
		cfg.addDefault(path, value);
		cfg.options().copyDefaults(true);
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void setValue(File file, String path, String value){
		
		FileConfiguration cfg = getConfiguration(file);
		
		cfg.set(path, value);
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static String getValue(File file, String path){
		FileConfiguration cfg = getConfiguration(file);
		return cfg.getString(path);
	}
	
	public static boolean getBoolean(File file, String path){
		FileConfiguration cfg = getConfiguration(file);
		return cfg.getBoolean(path);
	}
}
